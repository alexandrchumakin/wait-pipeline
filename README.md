# wait-pipeline

This project explains how to run pipelines for a project sequentially and make sure the next pipeline will not crash ongoing one

For motivation and details reach out [here](https://medium.com/@achumakin/wait-pipelines-in-gitlab-6a2f91432e77).
